		<!-- END CONTENT -->
		</div> 
		<div class="footerDiv">
				<div class="card-footer text-muted footerBackground">
		    	<h8 class="footerText">Created by Robert JF</h8>
		  	</div>
		</div>
	</body>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<script type="text/javascript">
		function logout(){
			$.ajax({
				url:"broker.php",
				type: "post",
				data: { "logout": "true" },
				success:function(result){
					window.location.href = '/php/website/index.php?msg='+result;
				}
			});
		}
	</script>
</html>
