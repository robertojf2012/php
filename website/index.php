<?php require_once "header.php";?>

<div class="container-fluid">

	<?php
		if(isset($_REQUEST['msg'])){
			echo '
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							<a id="resultText" class="alert-link">'.$_REQUEST['msg'].'</a>
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>
					</div>
				</div> 
			';
		}
	?>

	<div class="row">
		<div class="col-md-12">
			<h2 style="color: white;" align="center">Login</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<form id="formLogin" method="post" action="broker.php">
				<input autofocus="autofocus" class="form-control" required="required" type="text" placeholder="UserName" name="loginUserName">
				<br>
				<input class="form-control" required="required" type="password" placeholder="Password" name="loginPassword">
				<input class="form-control" type="hidden" name="route" value="<?=$_SERVER["PHP_SELF"]?>">
				<br>
				<button class="btn btn-primary btn-lg" type="submit">Login</button>
			</form>
		</div>
	</div>
</div>

<?php require_once "footer.php";