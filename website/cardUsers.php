<?php
	//connection to database
	$connection = connectDB();

	//getting the users
	$users = getUsers($connection);

	if(isset($_REQUEST['msg'])){
		echo '
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<a id="resultText" class="alert-link">'.$_REQUEST['msg'].'</a>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
				</div>
			</div>
		';
	}
?>

<div class="card cardBackground">
	<div class="card-body">
    <h5 class="card-title textTitle">Registered Users </h5>
		<table class="table table-hover table-dark">
		  <thead>
		    <tr>
		      <th scope="col">First Name</th>
		      <th scope="col">Last Name</th>
		      <th scope="col">User Name</th>
		      <th scope="col">Email</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php 
					while($row = mysqli_fetch_array($users))
					{
						echo '<tr>';
							echo '<td>'.$row['firstName'].'</td>';
							echo '<td>'.$row['lastName'].'</td>';
							echo '<td>'.$row['username'].'</td>';
							echo '<td>'.$row['email'].'</td>';
							echo '<td>'.
											'<div class="btn-group" role="group" aria-label="Basic example">'.
												'<button type="button" onclick="editUser('.$row['id'].')" class="btn btn-secondary"><i class="fas fa-edit"></i> Edit</button>'.
												'<button type="button" onclick="deleteUser('.$row['id'].',\''.$row['firstName'].'\')" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</button>'.
											'</div>'.
										'</td>';
						echo '</tr>';
					}
		  	?>
		  </tbody>
		</table>
  </div>
</div>

<!-- MODAL -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content backgroundColorModal">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="color: white;">Delete User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="color: white;">
      	Are you sure you want to delete <a id="userText"></a> ?
      </div>
      <div class="modal-footer">
      	<input type="hidden" class="form-control" id="idDelete" name="idDelete">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" onclick="confirmDelete()" class="btn btn-danger">Yes. Delete</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

	function editUser(idUser) {
		$.ajax({
			url:"broker.php",
			type: "post",
			data: { "idUser": idUser },
			success:function(result){
				var data = JSON.parse(result);
				console.log(data);
				$('#firstname').val(data.firstName);
				$('#lastname').val(data.lastName);
				$('#email').val(data.email);
				$('#username').val(data.username);
				$('#pass1').val(data.pass);
				$('#pass2').val(data.pass);
				$('#idUpdate').val(data.id);
				
				//showing appropiate buttons
				$("#btnRegister").attr("hidden",true);
				$('#btnEdit').removeAttr('hidden');
				$('#btnEdit').prop("disabled",false);
				$('#btnClean').removeAttr('hidden');
				$('#titleForm').text("Update User");
			}
		});
	}

	function deleteUser(idUser,name){
		$('#userText').text(name);
		$('#idDelete').val(idUser);
	}

	function confirmDelete(){
		var id = $('#idDelete').val();
		$.ajax({
			url:"broker.php",
			type: "post",
			data: { "idDelete": id },
			success:function(result){
				window.location.href = '/php/website/home.php?msg='+result;
			}
		});
	}

</script>