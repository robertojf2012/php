<?php
	require_once 'functions/functionsDB.php';
	session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>The website</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	</head>
	<body class="darkBackground">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <a class="navbar-brand" href="#">
		  	<img src="assets/people.png" width="40" height="40" alt="">
			</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link">
		        	<?php
		        		if(isset($_SESSION['username'])){
		        			echo $_SESSION['firstName'] .' '. $_SESSION['lastName'];
		        		}else{
		        			echo "Home";
		        		}
		        	?>
		        	<span class="sr-only">(current)</span></a>
		      </li>
		      <?php  
		      	if(isset($_SESSION['username'])){
        			echo'
								<li class="nav-item">
									<a style="cursor:pointer;" class="nav-link" onclick="logout()">Logout</a>
								</li>';
        		}
		      ?>
		    </ul>
		    
		    <form class="form-inline my-2 my-lg-0">
		      <input class="form-control mr-sm-2" type="search" placeholder="Search User" aria-label="Search User">
		      <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
		    </form>
		  </div>
		</nav>
		<div class="divider"></div>
		
		<!-- START CONTENT -->
		<div class="contentDiv"> 