<?php require_once "header.php";?>

<?php
	if(!isset($_SESSION['username'])){
		header("Location: /php/website/index.php?msg=You need to login first!");
	}
?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<?php require_once "cardUsers.php";?>
			</div>
			<div class="col-md-4">
				<?php require_once "cardRegister.php";?>
			</div>
		</div>
	</div>
<?php require_once "footer.php";?>
<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
	  'use strict';
	  window.addEventListener('load', function() {
	    // Fetch all the forms we want to apply custom Bootstrap validation styles to
	    var forms = document.getElementsByClassName('needs-validation');
	    // Loop over them and prevent submission
	    var validation = Array.prototype.filter.call(forms, function(form) {
	      form.addEventListener('submit', function(event) {
	        if (form.checkValidity() === false) {
	          event.preventDefault();
	          event.stopPropagation();
	        }
	        form.classList.add('was-validated');
	      }, false);
	    });
	  }, false);
	})();
	
	function validatePasswords(){
		var password1 = document.getElementById("pass1").value;
		var password2 = document.getElementById("pass2").value;
		if(password1 == password2){
			document.getElementById("btnRegister").disabled = false;
			document.getElementById("btnEdit").disabled = false;
		}else{
			document.getElementById("btnRegister").disabled = true;
			document.getElementById("btnEdit").disabled = true;

		}
	}
</script>

