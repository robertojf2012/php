<?php
	
	require_once 'functions/functions.php';
	require_once 'functions/functionsDB.php';

	if(isset($_REQUEST['route'])) {

		$isroute = is_string($_REQUEST['route']);
		
		if($isroute){

			$route = $_REQUEST['route'];

			switch ($route) {
				
				//login
				case "/php/website/index.php":

					if(isset($_REQUEST['loginUserName'])){
						$username = sanitize($_REQUEST['loginUserName']);
					}else{
						$username = '';
					}
					if(isset($_REQUEST['loginPassword'])){
						$password = sanitize($_REQUEST['loginPassword']);
					}else{
						$password = '';
					}

					//connection to database
					$connection = connectDB();
					$message = '';

					//do login
					if(login($connection,$username,$password)){
						$page = '/php/website/home.php';
					}else{
						$page = '/php/website/index.php';
						$message = 'Username or password incorrect';
					}

					goToPage($page,$message);

				break;

				//register or update user 
				case "/php/website/home.php":

					if(isset($_REQUEST['firstname'])){
						$firstname = sanitize($_REQUEST['firstname']);
					}else{
						$firstname = '';
					}
					if(isset($_REQUEST['lastname'])){
						$lastname = sanitize($_REQUEST['lastname']);
					}else{
						$lastname = '';
					}
					if(isset($_REQUEST['email'])){
						$email = sanitize($_REQUEST['email']);
					}else{
						$email = '';
					}
					if(isset($_REQUEST['username'])){
						$username = sanitize($_REQUEST['username']);
					}else{
						$username = '';
					}
					if(isset($_REQUEST['password'])){
						$password = sanitize($_REQUEST['password']);
					}else{
						$password = '';
					}

					//connection to database
					$connection = connectDB();

					//checking if it was update or register
					if(isset($_REQUEST['idUpdate']) && !empty($_REQUEST["idUpdate"])){
						$idUser = sanitize($_REQUEST['idUpdate']);
						
						//do update
						updateUser($connection,$idUser,$firstname,$lastname,$email,$username,$password);
						$page = '/php/website/home.php';
						$message = 'Usuario actualizado correctamente';

					}else{
						//do register
						registerUser($connection,$firstname,$lastname,$email,$username,$password);
						$page = '/php/website/home.php';
						$message = 'Usuario registrado correctamente';
					}

					goToPage($page,$message);

				break;

				default:
					echo "No access";
				break;

			}
		}
		
	}

	if (isset($_REQUEST['idUser'])) {
		$id = sanitize($_REQUEST['idUser']);
		$connection = connectDB();
		echo getUser($connection,$id);
	}

	if (isset($_REQUEST['idDelete'])) {
		$id = sanitize($_REQUEST['idDelete']);
		$connection = connectDB();
		deleteUser($connection,$id);
		echo'Usuario eliminado correctamente';
	}

	if (isset($_REQUEST['logout'])) {
		deleteSession();
		echo 'Session closed';
	}