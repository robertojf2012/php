<div class="card cardBackground">
	<div class="card-body">
		<h5 class="card-title textTitle" id="titleForm">Register User</h5>
		<form class="needs-validation" method="post" action="broker.php" novalidate>
		  <div class="form-row">
		    <div class="col-md-12 mb-3">
		      <label for="validationCustom01">First Name</label>
		      <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First name" required>
		      <div class="valid-feedback">
		        Looks good!
		      </div>
		    </div>
		    <div class="col-md-12 mb-3">
		      <label for="validationCustom02">Last Name</label>
		      <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last name" required>
		      <div class="valid-feedback">
		        Looks good!
		      </div>
		    </div>
		    <div class="col-md-12 mb-3">
		      <label for="validationCustom01">Email</label>
		      <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
		      <div class="valid-feedback">
		        Looks good!
		      </div>
		      <div class="invalid-feedback">
						Please write a valid email.
					</div>
		    </div>
		    <div class="col-md-12 mb-3">
		      <label for="validationCustomUsername">Username</label>
		      <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
		      <div class="valid-feedback">
		        Looks good!
		      </div>
		      <div class="invalid-feedback">
						Please write a username.
					</div>
		    </div>
		    <div class="col-md-12 mb-3">
		      <label for="validationCustom01">Password</label>
		      <input onchange="validatePasswords()" type="password" class="form-control" id="pass1" name="password" placeholder="Password" required>
		      <div class="valid-feedback">
		        Looks good!
		      </div>
		    </div>
		    <div class="col-md-12 mb-3">
		      <label for="validationCustom01">Repeat your password</label>
		      <input onchange="validatePasswords()" type="password" class="form-control" id="pass2" name="password2" placeholder="Repeat password" required>
		      <div class="valid-feedback">
		        Looks good!
		      </div>
		    </div>
		    <input class="form-control" type="hidden" name="route" value="<?=$_SERVER["PHP_SELF"]?>">
		    <input type="hidden" class="form-control" id="idUpdate" name="idUpdate">
		  </div>
		  <br>
		  <button id="btnRegister" class="btn btn-primary" disabled="disabled" type="submit">Register</button>
		  <button id="btnEdit" class="btn btn-info" hidden="hidden" disabled="disabled" type="submit">Update</button>
		  <button id="btnClean" class="btn btn-light" onclick="cleanData()" hidden="hidden" type="button">Clean</button>
		</form>
	</div>
</div>

<script type="text/javascript">

	function cleanData() {
		$('#firstname').val("");
		$('#lastname').val("");
		$('#email').val("");
		$('#username').val("");
		$('#pass1').val("");
		$('#pass2').val("");
		$('#idUpdate').val("");

		//showing appropiate buttons
		$('#btnRegister').prop("disabled",true);
		$('#btnRegister').removeAttr('hidden');
		$('#btnEdit').prop("disabled",true);
		$("#btnEdit").attr("hidden",true);

		$('#btnClean').attr('hidden',true);
		$('#titleForm').text("Register User");
	}

</script>

