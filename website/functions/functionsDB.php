<?php

	function connectDB(){
		$dbHost="localhost";
		$dbUser="root";
		$dbPass="135645";
		$db = "websitedb";
		$conn = mysqli_connect($dbHost, $dbUser, $dbPass, $db);
		if(mysqli_connect_errno()){
			$conn = "Error: " . mysqli_connect_error();
		}else{
			mysqli_query($conn, "SET NAMES 'utf8'");
		}
		return $conn;
	}
	
	function login($con, $username, $pass){
		$query = "CALL loginUser('$username','$pass');";
		$dataSet = mysqli_query($con, $query);
		$row = mysqli_fetch_assoc($dataSet);
		$flag = false;
		if($row != null){
			session_start();
			$_SESSION['username'] = $username;
			$_SESSION['firstName'] = $row['firstName'];
			$_SESSION['lastName'] = $row['lastName'];
			$_SESSION['email'] = $row['email'];
			$flag = true;
		}
		return $flag;
	}

	function registerUser($con,$firstname,$lastname,$email,$username,$password){
		$query = "CALL createUser('$firstname','$lastname','$email','$username','$password');";
		$dataSet = mysqli_query($con, $query);
	}

	function updateUser($con,$iduser,$firstname,$lastname,$email,$username,$password){
		$query = "CALL updateUser('$iduser','$firstname','$lastname','$email','$username','$password');";
		$dataSet = mysqli_query($con, $query);
	}

	function getUsers($con){
		$query = "CALL getUsers();";
		$dataSet = mysqli_query($con, $query);
		return $dataSet;
	}

	function getUser($con,$idUser){
		$query = "CALL getUser('$idUser');";
		$dataSet = mysqli_query($con, $query);
		$data = json_encode(mysqli_fetch_assoc($dataSet));
		return $data;
	}

	function deleteUser($con,$iduser){
		$query = "CALL deleteUser('$iduser');";
		$dataSet = mysqli_query($con, $query);
	}

	