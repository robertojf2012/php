<?php

	function sanitize($var){
		$variable = gettype($var);
		switch ($variable) {
			case 'boolean':
				$result = (boolean) $var;
				break;
			case 'integer':
				$result = (int) $var;
				break;
			case 'double':
				$result = (int) $var;
				break;
			case 'string':
				$result = htmlentities($var, ENT_QUOTES, "UTF-8");
				break;
			case 'array':
				$result = (array) $var;
				break;
			case 'object':
				$result = null;
				break;
			case 'resource':
				$result = null;
				break;
			case 'NULL':
				$result = null;
				break;
			default:
				$result = null;
				break;
		}
		return $result;
	}

	function goToPage($route,$message = false){
		if($message){
			header("Location: $route?msg=$message");
		}else{
			header("Location: $route");
		}
		exit();
	}

	function createSession(){
		session_start();
	}

	function deleteSession(){
		session_start();
		$_SESSION = array();
		session_destroy();
	}