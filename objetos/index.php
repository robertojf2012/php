<?php
	require_once 'Chevy.php';
	require_once 'Database.php';
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Objetos en PHP</title>
	</head>
	<body>
	  <?php
	    
	    $Auto = new Chevy(70,4,12560,5,"Roberto JF");
	    
	    echo "PROPIETARIO = ".$Auto->propietario."<br>";
	    echo "VELOCIDAD = ".$Auto->getvelocidad()."Km/h <br>";
	    echo "KILOMETRAJE = ".$Auto->getKilometraje()." <br>";
	    echo $Auto->start();
	    echo "<br>";
	    echo "TIPO DE ENCENDIDO = ".$Auto->getEncendido();
	    echo "<br>";
	    var_dump($Auto);

	    echo "<br><br><br><br><br>";

	    $database = new Database('localhost','root','135645','websitedb');

			echo $database->connected();

	    //var_dump($database);

	   ?>
	</body>
</html>