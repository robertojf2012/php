<?php 

	class Vehiculo {

		public $propietario;
		private $velocidad;
		private $numPuertas;
		private $kilometraje;
		
		public function getvelocidad(){
			return $this->velocidad;
		}

		public function setVelocidad($param){
			$this->velocidad = $param;
		}

		public function getNumPuertas(){
			return $this->numPuertas;
		}

		public function setNumPuertas($param){
			$this->numPuertas = $param;
		}

		public function getKilometraje(){
			return $this->kilometraje;
		}

		public function setKilometraje($param){
			$this->kilometraje = $param;
		}

	}