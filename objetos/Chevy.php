<?php

	require_once 'Carro.php';
	
	class Chevy extends Carro {

		public function __construct($velocidad, $numPuertas, $kilometraje, $pasajeros, $propietario) {
			$this->setVelocidad($velocidad);
			$this->setNumPuertas($numPuertas);
			$this->setKilometraje($kilometraje);
			$this->propietario = $propietario;
			$this->setEncendido("chevy");
		}

		public function start() {
			return "encendiendo chevy";
		}

	}