<?php

	class Database
	{
		private $host;
    private $user;
    private $password;
    private $db;
    private $mysqli;
		
		function __construct($host,$user,$pass,$db)
		{
			$this->host = $host;
			$this->user = $user;
			$this->pass = $pass;
			$this->db = $db;
			$this->mysqli = new mysqli($this->host, $this->user, $this->pass, $this->db);
		}

		public function connected()
		{
			if($this->connect_error) {
				die('Connect Error ('.$this>connect_errno.')'.$this->connect_error);
			}
			if (mysqli_connect_error()) {
				die('Connect Error ('.mysqli_connect_errno().')'.mysqli_connect_error());
			}
			echo 'Connection Success...'.$this->host_info."\n";
		}

		public function closeConnection()
		{
			$this->mysqli->close();
			return 'ok';
		}

		public function query($query)
    {
    	return $this->mysqli->query($query);
    }
	}

?>