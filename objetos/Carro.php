<?php 

	require_once 'Vehiculo.php';

	abstract class Carro extends Vehiculo {

		private $encendido;
		
		public abstract function start();

		public function setEncendido($strAtributo){
  		$this->encendido = $strAtributo;
    }

    public function getEncendido(){
  		return $this->encendido;
    }


	}