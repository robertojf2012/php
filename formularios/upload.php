<?php 

	include 'functionsDB.php';
	$statusMsg = '';

	$targetDir = "images/";
	$fileName = basename($_FILES["imagen"]["name"]); //obtenemos el nombre del archivo
	$targetFilePath = $targetDir . $fileName;
	$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);


	if(isset($_POST["submit"]) && !empty($_FILES["imagen"]["name"])){

    $allowTypes = array('jpg','png','jpeg','gif');
    
    //Revisa que el tipo de formato del archivo este en el arreglo de los permitidos
    if(in_array($fileType, $allowTypes)){
        
        //Subimos el archivo a la ruta indicada
        if(move_uploaded_file($_FILES["imagen"]["tmp_name"], $targetFilePath)){
            
          //Subir imagen a la BD
        	try{
        		$connection = connectDB();
        		$query = "CALL uploadImage('$targetFilePath');";
        		$dataSet = mysqli_query($connection, $query);
            $statusMsg = "Archivo subido exitosamente";
        	}catch(Exception $e){
        		echo $e->getMessage();
        	}

        }else{
            $statusMsg = "Ocurrio un error";
        }
    }else{
        $statusMsg = 'Solo archivos de imagen JPG, JPEG, PNG, GIF son permitidos';
    }
	}else{
	    $statusMsg = 'Selecciona una foto para subir';
	}

	//Muestra el mensaje
	echo $statusMsg;

	//Get images
	$connection = connectDB();
	$query2 = "CALL getImages();";
	$dataSet2 = mysqli_query($connection, $query2);
  $lista = mysqli_fetch_all($dataSet2, MYSQLI_ASSOC);
?>
	<br>
<?php 
  foreach ($lista as $imagen) {
 ?>
  	<img width="600" height="400" src="<?php echo $imagen['image'];?>" alt="" />
<?php 
  }
?>


