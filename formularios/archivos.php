<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
</body>
</html>

<?php
	$route = './images';
	$dir = opendir($route);
	if ($dir){
		$count = 1;
		echo "
			<table class='table table-striped table-dark'>
				<tbody>
					<tr>
					";
		while(($file = readdir($dir)) !== false){
			
			if(strlen($file) > 2){
				if($count == 4){
					echo "
						<tr></tr>
					";
					$count = 0;
				}else{
					echo "
						<td><img src='images/$file' width='200px'/><br/></td>
					";
				}
				$count++;
			}

		}
		echo "
					</tr>
				</tbody>
			</table>
		";
	}

?>
