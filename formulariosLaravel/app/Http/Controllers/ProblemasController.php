<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProblemasController extends Controller
{
    public function getSueldoSemanal(Request $request){
    	$strHoras = $request->input('horas');
    	$strPago = $request->input('pago');

    	$horas = (int) $strHoras;
    	$pago = (float) $strPago;

      if($horas > 40){
      	$sueldo = ($horas*$pago)*2;
      }else{
      	$sueldo = $horas*$pago;
      }
      echo "PAGO SEMANAL = ".$sueldo;
    }

    public function getRegalo(Request $request){
    	$strMoney = $request->input('money');
    	$money = (float) $strMoney;

    	if($money <= 10){
    		$regalo = "Tarjeta";
    	}

    	if($money <= 100 && $money >= 11){
    		$regalo = "Chocolates";
    	}

    	if($money <= 250 && $money >= 101){
    		$regalo = "Flores";
    	}

    	if($money >= 251){
    		$regalo = "Joyas";
    	}

    	return $regalo;
    }

    public function getCostoDescuento(Request $request){
    	$precio = $request->input('precio');
			
			if($precio >= 200){
				$descuento = .15;
			}

			if($precio > 100 && $precio < 200){
				$descuento = .12;
			}

			if($precio < 100){
				$descuento = .10;
			}

			$costo = $precio - ($precio * $descuento);

			return ['costo' => $costo, 'descuento' => $descuento*100];
    }

    public function getBeca(Request $request){
    	$edad = (double)$request->input('edad');
    	$promedio = (double)$request->input('promedio');
			
    	$beca = 0;
    	$mensaje = '';

			if($edad>18){
				if($promedio>=9){
					$beca = 2000;
				}else{
					if($promedio>=7.5){
						$beca = 1000;
					}else{
						if($promedio<7.5 && $promedio>=6){
							$beca = 500;
						}else{
							$mensaje = 'Estudia mas!!';
						}
					}
				}
			}else{
				if($promedio>=9){
					$beca = 3000;
				}else{
					if($promedio<9 && $promedio>=8){
						$beca = 2000;
					}else{
						if($promedio<8 && $promedio>=6){
							$beca = 100;
						}else{
							$mensaje = 'Estudia mas!!';
						}
					}
				} 
			}

			return ['beca' => $beca, 'mensaje' => $mensaje];
    }
}
