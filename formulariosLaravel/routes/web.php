<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/p1', function () {
    return view('problema1');
});

Route::get('/p2', function () {
    return view('problema2');
});

Route::get('/p3', function () {
    return view('problema3');
});

Route::get('/p4', function () {
    return view('problema4');
});

Route::post('/sueldoSemanal',"ProblemasController@getSueldoSemanal");
Route::post('/regalo',"ProblemasController@getRegalo");
Route::post('/costoDescuento',"ProblemasController@getCostoDescuento");
Route::post('/beca',"ProblemasController@getBeca");