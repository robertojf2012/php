<!DOCTYPE html>
<html>
	<head>
		<title>Problema 2</title>
		<link href="css/app.css" rel="stylesheet">
	</head>
	<body>
		<form>
			@csrf
		  <div class="form-group">
		    <label>Dinero</label>
		    <input type="number" class="form-control" name="money" id="money">
		  </div>
		  <button onclick="getRegalo(document.getElementById('money').value)" type="button" class="btn btn-primary">Calcular</button>
		</form>
		<div class="row" style="padding-top: 20px;"></div>
		<div class="alert alert-success" role="alert">
		  <b>Regalo: </b><a id="txtRegalo"></a>
		</div>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</html>

<script type="text/javascript">
	function getRegalo(money) {
		$.ajax({
	    url: "{{ url('regalo')}}",
	    method: 'post',
	    data: {'money': money, '_token': '{{ csrf_token() }}'},
	    success: function (response) {
	    	document.getElementById("txtRegalo").text = response;
	    },
	    error: function (jqXHR, exception) {
	        console.log(jqXHR);
	    },
		});
	}
</script>