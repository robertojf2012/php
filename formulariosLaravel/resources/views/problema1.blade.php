<!DOCTYPE html>
<html>
	<head>
		<title>Problema 1</title>
		<link href="css/app.css" rel="stylesheet">
	</head>
	<body>
		<form method="post" action="{{ url('sueldoSemanal')}}">
			@csrf
		  <div class="form-group">
		    <label>Horas trabajadas</label>
		    <input type="number" class="form-control" name="horas">
		  </div>
		  <div class="form-group">
		    <label>Pago por hora</label>
		    <input type="number" class="form-control" name="pago">
		  </div>
		  <button type="submit" class="btn btn-primary">Calcular</button>
		</form>
		<div class="row" style="padding-top: 20px;"></div>
		<!--
		<div class="alert alert-success" role="alert">
		  <b>Pago: </b>
		</div>
		-->
	</body>
</html>