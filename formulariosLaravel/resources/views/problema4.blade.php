<!DOCTYPE html>
<html>
	<head>
		<title>Problema 4</title>
		<link href="css/app.css" rel="stylesheet">
	</head>
	<body>
		<form action="#" method="post" id="form">
			@csrf
		  <div class="form-group">
		    <label>Edad</label>
		    <input type="number" class="form-control" name="edad">
		  </div>
		  <div class="form-group">
		    <label>Promedio</label>
		    <input type="number" class="form-control" name="promedio">
		  </div>
		  <button type="submit" class="btn btn-primary">Calcular BECA</button>
		</form>
		<div class="row" style="padding-top: 20px;"></div>
		<div class="alert alert-success" role="alert">
		  <b>Beca asiganda: </b><a id="txtResponse"></a>
		</div>
		<div class="alert alert-warning" role="alert">
			<b><a id="txtMensaje"></a></b>
		</div>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</html>

<script type="text/javascript">
	$("#form").submit(function(e) {
    e.preventDefault();
    var form = $(this);

    $.ajax({
    	url: "{{ action('ProblemasController@getBeca') }}",
			method: "post",
			data: form.serialize(), // serializes the form's elements.
			success: function (response) {
				console.log(response);
	    	document.getElementById("txtResponse").text = response.beca;
	    	document.getElementById("txtMensaje").text = response.mensaje;
	    },
	    error: function (jqXHR, exception) {
	    	console.log(jqXHR);
	    }
	  });

	});
</script>