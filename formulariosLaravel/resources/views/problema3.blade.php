<!DOCTYPE html>
<html>
	<head>
		<title>Problema 3</title>
		<link href="css/app.css" rel="stylesheet">
	</head>
	<body>
		<form action="#" method="post" id="form">
			@csrf
		  <div class="form-group">
		    <label>Precio</label>
		    <input type="number" class="form-control" name="precio">
		  </div>
		  <button type="submit" class="btn btn-primary">Calcular</button>
		</form>
		<div class="row" style="padding-top: 20px;"></div>
		<div class="alert alert-success" role="alert">
		  <b>Costo: </b><a id="txtCosto"></a>
		</div>
		<div class="alert alert-success" role="alert">
		  <b>Descuento aplicado: </b><a id="txtDescuento"></a>
		</div>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</html>

<script type="text/javascript">
	$("#form").submit(function(e) {
    e.preventDefault();
    var form = $(this);

    $.ajax({
    	url: "{{ action('ProblemasController@getCostoDescuento') }}",
			method: "post",
			data: form.serialize(), // serializes the form's elements.
			success: function (response) {
	    	document.getElementById("txtCosto").text = response.costo;
	    	document.getElementById("txtDescuento").text = response.descuento + '%';
	    	//console.log(response);
	    },
	    error: function (jqXHR, exception) {
	    	console.log(jqXHR);
	    }
	  });
	  
	});
</script>